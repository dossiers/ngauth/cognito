import { Injectable } from "@angular/core";
import {
  AuthenticationDetails,
  CognitoIdentityServiceProvider,
  CognitoUser,
  CognitoUserAttribute,
  CognitoUserPool,
  ICognitoUserPoolData
} from "amazon-cognito-identity-js";
import * as AWS from "aws-sdk/global";
// import * as AWS from "aws-sdk";
import * as awsservice from "aws-sdk/lib/service";
import * as CognitoIdentity from "aws-sdk/clients/cognitoidentity";

import { DevLogger as dl } from '@ngcore/core'; import isDL = dl.isLoggable;
import { CognitoCallback, LoggedInCallback, CommonCallback } from '@ngauth/core';
import { ICognitoAuthConfig } from '@ngauth/core';
import { AuthConfigManager } from '@ngauth/services';
import { AwsCognitoUtil } from './common/aws-cognito-util';
import { CognitoIdentityCredentials } from "aws-sdk/lib/credentials/cognito_identity_credentials";


@Injectable()
export class CognitoCommonUtil {

  private _authConfig: (ICognitoAuthConfig | null) = null;
  constructor(private authConfigManager: AuthConfigManager) {
    this._initAuthConfig();
  }
  private _initAuthConfig() {
    this.authConfigManager.loadConfig().subscribe((authConfig) => {
      this._authConfig = authConfig;
    });
  }
  public get isInitialized(): boolean {
    return !!this._authConfig;
  }


  // TBD:
  // Temporarily checking this.isInitialized.
  // These apis should really be changed to async...
  // .....

  public get authConfig(): ICognitoAuthConfig {
    if (!this.isInitialized) {
      throw new Error("CognitoCommonUtil.authConfig not initialized");
    }
    return this._authConfig;
  }


  private userPool: (CognitoUserPool | null) = null;
  public getUserPool(): CognitoUserPool {
    if (!this.userPool) {
      if (!this.isInitialized) {
        throw new Error("CognitoCommonUtil.authConfig not initialized");
      }
      let poolData: ICognitoUserPoolData = {
        UserPoolId: this._authConfig.userPoolId,
        ClientId: this._authConfig.userPoolClientId
      };
      if (this._authConfig.cognitoIdpEndpoint) {
        poolData.endpoint = this._authConfig.cognitoIdpEndpoint;
      }
      this.userPool = new CognitoUserPool(poolData);
    }
    return this.userPool;
  }

  public getCurrentUser(): CognitoUser {
    // if (!this.isInitialized) {
    //   throw new Error("CognitoCommonUtil.authConfig not initialized");
    // }
    return this.getUserPool().getCurrentUser();
  }


  private cognitoCreds: AWS.CognitoIdentityCredentials;
  
  // AWS Stores Credentials in many ways, and with TypeScript this means that 
  // getting the base credentials we authenticated with from the AWS globals gets really murky,
  // having to get around both class extension and unions. Therefore, we're going to give
  // developers direct access to the raw, unadulterated CognitoIdentityCredentials
  // object at all times.
  private setCognitoCreds(creds: AWS.CognitoIdentityCredentials) {
    // if (!this.isInitialized) {
    //   throw new Error("CognitoCommonUtil.authConfig not initialized");
    // }
    this.cognitoCreds = creds;
  }

  // ???
  private getCognitoCreds(): AWS.CognitoIdentityCredentials {
    // if (!this.isInitialized) {
    //   throw new Error("CognitoCommonUtil.authConfig not initialized");
    // }
    // tbd:
    return this.cognitoCreds;
  }

  // This method takes in a raw jwtToken and uses the global AWS config options to build a
  // CognitoIdentityCredentials object and store it for us. It also returns the object to the caller
  // to avoid unnecessary calls to setCognitoCreds.

  public buildCognitoCreds(idTokenJwt: string) {
    if (!this.isInitialized) {
      throw new Error("CognitoCommonUtil.authConfig not initialized");
    }
    let url: string;
    if (this._authConfig.cognitoIdpEndpoint) {
      url = AwsCognitoUtil.getCognitoIdpEndpointUrl(this._authConfig.cognitoIdpEndpoint, this._authConfig.userPoolId);
    } else {
      url = AwsCognitoUtil.getDefaultCognitoIdpEndpointUrl(this._authConfig.region, this._authConfig.userPoolId);
    }

    let logins: CognitoIdentity.LoginsMap = {};
    logins[url] = idTokenJwt;
    let params = {
      IdentityPoolId: this._authConfig.identityPoolId, /* required */
      Logins: logins
    };
    let serviceConfigs: awsservice.ServiceConfigurationOptions = {};
    if(this._authConfig.coginitoIdentityEndpoint) {
      serviceConfigs.endpoint = this._authConfig.coginitoIdentityEndpoint;
    }
    // tbd:
    let creds = new AWS.CognitoIdentityCredentials(params, serviceConfigs);
    this.setCognitoCreds(creds);
    return creds;
    // return null;
  }


  public getCognitoIdentity(): string {
    // if (!this.isInitialized) {
    //   throw new Error("CognitoCommonUtil.authConfig not initialized");
    // }
    return this.cognitoCreds.identityId;
  }

  public getAccessToken(callback: CommonCallback): void {
    if (!this.isInitialized) {
      throw new Error("CognitoCommonUtil.authConfig not initialized");
    }
    if (callback == null) {
      throw ("CognitoUtil: callback in getAccessToken is null...returning");
    }
    if (this.getCurrentUser() != null)
      this.getCurrentUser().getSession(function (err, session) {
        if (err) {
          console.log("CognitoUtil: Can't set the credentials:" + err);
          callback.callbackWithParam(null);
        }

        else {
          if (session.isValid()) {
            callback.callbackWithParam(session.getAccessToken().getJwtToken());
          }
        }
      });
    else {
      callback.callbackWithParam(null);
    }
  }

  public getIdToken(callback: CommonCallback): void {
    if (!this.isInitialized) {
      throw new Error("CognitoCommonUtil.authConfig not initialized");
    }
    if (callback == null) {
      throw ("CognitoUtil: callback in getIdToken is null...returning");
    }
    if (this.getCurrentUser() != null)
      this.getCurrentUser().getSession(function (err, session) {
        if (err) {
          console.log("CognitoUtil: Can't set the credentials:" + err);
          callback.callbackWithParam(null);
        }
        else {
          if (session.isValid()) {
            callback.callbackWithParam(session.getIdToken().getJwtToken());
          } else {
            console.log("CognitoUtil: Got the id token, but the session isn't valid");
          }
        }
      });
    else {
      callback.callbackWithParam(null);
    }
  }

  private getRefreshToken(callback: CommonCallback): void {
    if (!this.isInitialized) {
      throw new Error("CognitoCommonUtil.authConfig not initialized");
    }
    if (callback == null) {
      throw ("CognitoUtil: callback in getRefreshToken is null...returning");
    }
    if (this.getCurrentUser() != null)
      this.getCurrentUser().getSession(function (err, session) {
        if (err) {
          console.log("CognitoUtil: Can't set the credentials:" + err);
          callback.callbackWithParam(null);
        }

        else {
          if (session.isValid()) {
            callback.callbackWithParam(session.getRefreshToken());
          }
        }
      });
    else {
      callback.callbackWithParam(null);
    }
  }

  private refresh(): void {
    if (!this.isInitialized) {
      throw new Error("CognitoCommonUtil.authConfig not initialized");
    }
    this.getCurrentUser().getSession(function (err, session) {
      if (err) {
        console.log("CognitoUtil: Can't set the credentials:" + err);
      }

      else {
        if (session.isValid()) {
          if(isDL()) dl.log("CognitoUtil: refreshed successfully");
        } else {
          if(isDL()) dl.log("CognitoUtil: refreshed but session is still not valid");
        }
      }
    });
  }
}
