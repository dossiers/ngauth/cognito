import { Injectable } from "@angular/core";
import * as AWS from "aws-sdk/global";

import { DevLogger as dl } from '@ngcore/core'; import isDL = dl.isLoggable;
import { CommonCallback } from '@ngauth/core';
import { AwsCognitoUtil }from './common/aws-cognito-util';
import { CognitoCommonUtil } from './cognito-common-util';
import { DdbServiceUtil } from './ddb-service-util';


// TBD:
// Is this being used (internally) at all ????
// No.
// ...

@Injectable()
export class AwsServiceHelper {
  // ???
  public static firstLogin: boolean = false;
  public static runningInit: boolean = false;

  constructor(
    private cognitoUtil: CognitoCommonUtil,
    private ddbUtil: DdbServiceUtil
  ) {
    AWS.config.region = cognitoUtil.authConfig.region;
  }

  // TBD:
  // All these methods need to be public.
  // Otherwise, what's the point of this class?
  // TBD:
  // Check each method and make it public..
  // (Some of the methods do not seem to make sense....)

  /**
   * This is the method that needs to be called in order to init the aws global creds
   */
  private initAwsService(callback: CommonCallback, isLoggedIn: boolean, idToken: string) {

    if (AwsServiceHelper.runningInit) {
      // Need to make sure I don't get into an infinite loop here, so need to exit if this method is running already
      if(isDL()) dl.log("AwsServiceHelper: Aborting running initAwsService()...it's running already.");
      // instead of aborting here, it's best to put a timer
      if (callback != null) {
        callback.callback();
        callback.callbackWithParam(null);
      }
      return;
    }


    if(isDL()) dl.log("AwsServiceHelper: Running initAwsService()");
    AwsServiceHelper.runningInit = true;


    let mythis = this;
    // First check if the user is authenticated already
    if (isLoggedIn)
      mythis.setupAWS(isLoggedIn, callback, idToken);

  }


  /**
   * Sets up the AWS global params
   *
   * @param isLoggedIn
   * @param callback
   */
  private setupAWS(isLoggedIn: boolean, callback: CommonCallback, idToken: string): void {
    if(isDL()) dl.log("AwsServiceHelper: in setupAWS()");
    if (isLoggedIn) {
      if(isDL()) dl.log("AwsServiceHelper: User is logged in");
      // Setup mobile analytics
      var options = {
        appId: '32673c035a0b40e99d6e1f327be0cb60',
        appTitle: "aws-cognito-angular2-quickstart"
      };

      // TODO: The mobile Analytics client needs some work to handle Typescript. Disabling for the time being.
      // var mobileAnalyticsClient = new AMA.Manager(options);
      // mobileAnalyticsClient.submitEvents();

      this.addCognitoCredentials(idToken);

      if(isDL()) dl.log("AwsServiceHelper: Retrieving the id token");

    }
    else {
      if(isDL()) dl.log("AwsServiceHelper: User is not logged in");
    }

    if (callback != null) {
      callback.callback();
      callback.callbackWithParam(null);
    }

    AwsServiceHelper.runningInit = false;
  }

  private addCognitoCredentials(idTokenJwt: string): void {
    let creds = this.cognitoUtil.buildCognitoCreds(idTokenJwt);

    AWS.config.credentials = creds;

    let that = this;  // ???
    creds.get(function (err) {
      if (!err) {
        if (AwsServiceHelper.firstLogin) {
          // save the login info to DDB
          that.ddbUtil.writeLogEntry("login");
          AwsServiceHelper.firstLogin = false;
        }
      }
    });
  }

  private getCognitoParametersForIdConsolidation(idTokenJwt: string): {} {
    if(isDL()) dl.log("AwsServiceHelper: enter getCognitoParametersForIdConsolidation()");

    // ????
    // let url = AwsCognitoUtil.getDefaultCognitoIdpEndpointUrl(this.cognitoUtil.authConfig.region, this.cognitoUtil.authConfig.userPoolId);
    let url: string;
    if (this.cognitoUtil.authConfig.cognitoIdpEndpoint) {
      url = AwsCognitoUtil.getCognitoIdpEndpointUrl(this.cognitoUtil.authConfig.cognitoIdpEndpoint, this.cognitoUtil.authConfig.userPoolId);
    } else {
      url = AwsCognitoUtil.getDefaultCognitoIdpEndpointUrl(this.cognitoUtil.authConfig.region, this.cognitoUtil.authConfig.userPoolId);
    }
    // ???

    let logins: Array<string> = [];
    logins[url] = idTokenJwt;
    let params = {
      IdentityPoolId: this.cognitoUtil.authConfig.identityPoolId, /* required */
      Logins: logins
    };

    return params;
  }

}
