import { Inject, Injectable } from "@angular/core";
import { AuthenticationDetails, CognitoUser, CognitoUserAttribute } from "amazon-cognito-identity-js";
import * as AWS from "aws-sdk/global";

import { DevLogger as dl } from '@ngcore/core'; import isDL = dl.isLoggable;
import { CognitoCallback } from '@ngauth/core';
import { UserRegistrationInfo } from '@ngauth/core';
import { CognitoCommonUtil } from './cognito-common-util';


@Injectable()
export class UserRegistrationHelper {

  constructor(private cognitoUtil: CognitoCommonUtil) {
  }

  register(user: UserRegistrationInfo, callback: CognitoCallback): void {
    if (isDL()) dl.log("UserRegistrationHelper: user is " + user);

    let attributeList = [];
    let dataEmail = {
      Name: 'email',
      Value: user.email
    };
    let dataNickname = {
      Name: 'nickname',
      Value: user.name
    };
    attributeList.push(new CognitoUserAttribute(dataEmail));
    attributeList.push(new CognitoUserAttribute(dataNickname));

    this.cognitoUtil.getUserPool().signUp(user.email, user.password, attributeList, null, function (err, result) {

      if (err) {
        callback.cognitoCallback(err.message, null);
      } else {
        if (dl.isLoggable()) dl.log("UserRegistrationHelper: registered user is " + result);
        callback.cognitoCallback(null, result);
      }
    });

  }

  confirmRegistration(username: string, confirmationCode: string, callback: CognitoCallback): void {

    let userData = {
      Username: username,
      Pool: this.cognitoUtil.getUserPool()
    };

    let cognitoUser = new CognitoUser(userData);

    cognitoUser.confirmRegistration(confirmationCode, true, function (err, result) {
      if (err) {
        callback.cognitoCallback(err.message, null);
      } else {
        callback.cognitoCallback(null, result);
      }
    });
  }

  resendCode(username: string, callback: CognitoCallback): void {
    let userData = {
      Username: username,
      Pool: this.cognitoUtil.getUserPool()
    };

    let cognitoUser = new CognitoUser(userData);

    cognitoUser.resendConfirmationCode(function (err, result) {
      if (err) {
        callback.cognitoCallback(err.message, null);
      } else {
        callback.cognitoCallback(null, result);
      }
    });
  }

  // newPassword(newPasswordInfo: NewPasswordInfo, callback: CognitoCallback): void {
  //    if(isDL()) dl.log("newPasswordInfo: " + newPasswordInfo.toString());

  //    // Get these details and call
  //     //cognitoUser.completeNewPasswordChallenge(newPassword, userAttributes, this);
  //     let authenticationData = {
  //         Username: newPasswordInfo.username,
  //         Password: newPasswordInfo.oldPassword,
  //     };
  //     let authenticationDetails = new AuthenticationDetails(authenticationData);

  //     let userData = {
  //         Username: newPasswordInfo.username,
  //         Pool: this.cognitoUtil.getUserPool()
  //     };

  //     if(isDL()) dl.log("UserRegistrationHelper: Params set...Authenticating the user");
  //     let cognitoUser = new CognitoUser(userData);
  //     if(isDL()) dl.log("UserRegistrationHelper: config is " + AWS.config);
  //     cognitoUser.authenticateUser(authenticationDetails, {
  //         newPasswordRequired: function (userAttributes, requiredAttributes) {
  //             // User was signed up by an admin and must provide new
  //             // password and required attributes, if any, to complete
  //             // authentication.

  //             // the api doesn't accept this field back
  //             delete userAttributes.email_verified;
  //             cognitoUser.completeNewPasswordChallenge(newPasswordInfo.newPassword, requiredAttributes, {
  //                 onSuccess: function (result) {
  //                     callback.cognitoCallback(null, userAttributes);
  //                 },
  //                 onFailure: function (err) {
  //                     callback.cognitoCallback(err, null);
  //                 }
  //             });
  //         },
  //         onSuccess: function (result) {
  //             callback.cognitoCallback(null, result);
  //         },
  //         onFailure: function (err) {
  //             callback.cognitoCallback(err, null);
  //         }
  //     });
  // }
}