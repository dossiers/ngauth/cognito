import { Injectable } from "@angular/core";
import { AuthenticationDetails, CognitoUser } from "amazon-cognito-identity-js";
import * as AWS from "aws-sdk/global";
import * as STS from "aws-sdk/clients/sts";

import { DevLogger as dl } from '@ngcore/core'; import isDL = dl.isLoggable;
import { CognitoCallback, LoggedInCallback } from '@ngauth/core';
import { NewPasswordInfo } from '@ngauth/core';
import { CognitoCommonUtil } from './cognito-common-util';
import { DdbServiceUtil } from './ddb-service-util';


@Injectable()
export class UserLoginHelper {

  constructor(
    private cognitoUtil: CognitoCommonUtil,
    private ddbUtil: DdbServiceUtil
  ) {
  }

  public isAuthenticated(callback: LoggedInCallback) {
    if (!callback) {
      throw ("UserLoginHelper: Callback in isAuthenticated() cannot be null");
    }

    let cognitoUser: (CognitoUser | null) = null;
    try {
      cognitoUser = this.cognitoUtil.getCurrentUser();
    } catch (ex) {
      console.log("UserLoginHelper: ex = " + ex);
      // callback.isLoggedIn("Can't retrieve the CurrentUser", false);
    }

    if (cognitoUser != null) {
      cognitoUser.getSession(function (err, session) {
        if (err) {
          console.log("UserLoginHelper: Couldn't get the session: " + err, err.stack);
          callback.isLoggedIn(err, false);
        }
        else {
          if(dl.isLoggable()) dl.log("UserLoginHelper: Session is " + session.isValid());
          callback.isLoggedIn(err, session.isValid());
        }
      });
    } else {
      if(dl.isLoggable()) dl.log("UserLoginHelper: can't retrieve the current user");
      callback.isLoggedIn("Can't retrieve the CurrentUser", false);
    }
  }

  authenticate(username: string, password: string, callback: CognitoCallback) {
    if(isDL()) dl.log("UserLoginHelper: starting the authentication")

    let authenticationData = {
      Username: username,
      Password: password,
    };
    let authenticationDetails = new AuthenticationDetails(authenticationData);

    let userData = {
      Username: username,
      Pool: this.cognitoUtil.getUserPool()
    };

    if(isDL()) dl.log("UserLoginHelper: Params set...Authenticating the user");
    let cognitoUser = new CognitoUser(userData);
    if(isDL()) dl.log("UserLoginHelper: config is " + AWS.config);
    // Note: JSON.stringify() in the following sometimes throws an exception:
    //       "TypeError: Converting circular structure to JSON".
    // if(isDL()) dl.log(">>> UserLoginHelper: config is " + JSON.stringify(AWS.config));
    var self = this;
    cognitoUser.authenticateUser(authenticationDetails, {
      newPasswordRequired: function (userAttributes, requiredAttributes) {
        callback.cognitoCallback(`User needs to set password.`, null);
      },
      onSuccess: function (result) {
        if(dl.isLoggable()) dl.log("In authenticateUser onSuccess callback");

        let creds = self.cognitoUtil.buildCognitoCreds(result.getIdToken().getJwtToken());

        AWS.config.credentials = creds;

        // ???
        // So, when CognitoIdentity authenticates a user, it doesn't actually hand us the IdentityID,
        // used by many of our other handlers. This is handled by some sly underhanded calls to AWS Cognito
        // API's by the SDK itself, automatically when the first AWS SDK request is made that requires our
        // security credentials. The identity is then injected directly into the credentials object.
        // If the first SDK call we make wants to use our IdentityID, we have a
        // chicken and egg problem on our hands. We resolve this problem by "priming" the AWS SDK by calling a
        // very innocuous API call that forces this behavior.
        let clientParams: any = {};
        if (self.cognitoUtil.authConfig.stsEndpoint) {
          clientParams.endpoint = self.cognitoUtil.authConfig.stsEndpoint;
        }
        let sts = new STS(clientParams);
        sts.getCallerIdentity(function (err, data) {
          if(dl.isLoggable()) dl.log("UserLoginHelper: Successfully set the AWS credentials");
          callback.cognitoCallback(null, result);
        });

      },
      onFailure: function (err) {
        callback.cognitoCallback(err.message, null);
      },
    });
  }

  forgotPassword(username: string, callback: CognitoCallback) {
    let userData = {
      Username: username,
      Pool: this.cognitoUtil.getUserPool()
    };

    let cognitoUser = new CognitoUser(userData);

    cognitoUser.forgotPassword({
      onSuccess: function () {

      },
      onFailure: function (err) {
        callback.cognitoCallback(err.message, null);
      },
      inputVerificationCode() {
        callback.cognitoCallback(null, null);
      }
    });
  }

  confirmNewPassword(email: string, verificationCode: string, password: string, callback: CognitoCallback) {
    let userData = {
      Username: email,
      Pool: this.cognitoUtil.getUserPool()
    };

    let cognitoUser = new CognitoUser(userData);

    cognitoUser.confirmPassword(verificationCode, password, {
      onSuccess: function () {
        callback.cognitoCallback(null, null);
      },
      onFailure: function (err) {
        callback.cognitoCallback(err.message, null);
      }
    });
  }


  changePassword(newPasswordInfo: NewPasswordInfo, callback: CognitoCallback): void {
    if(isDL()) dl.log("newPasswordInfo: " + newPasswordInfo.toString());

    // Get these details and call
    //cognitoUser.completeNewPasswordChallenge(newPassword, userAttributes, this);
    let authenticationData = {
      Username: newPasswordInfo.username,
      Password: newPasswordInfo.oldPassword,
    };
    let authenticationDetails = new AuthenticationDetails(authenticationData);

    let userData = {
      Username: newPasswordInfo.username,
      Pool: this.cognitoUtil.getUserPool()
    };

    if(isDL()) dl.log("UserRegistrationHelper: Params set...Authenticating the user");
    let cognitoUser = new CognitoUser(userData);
    if(isDL()) dl.log("UserRegistrationHelper: config is " + AWS.config);
    // cognitoUser.authenticateUser(authenticationDetails, {
    //   newPasswordRequired: function (userAttributes, requiredAttributes) {
    //     // User was signed up by an admin and must provide new
    //     // password and required attributes, if any, to complete
    //     // authentication.

    //     // the api doesn't accept this field back
    //     delete userAttributes.email_verified;
    //     cognitoUser.completeNewPasswordChallenge(newPasswordInfo.newPassword, requiredAttributes, {
    //       onSuccess: function (result) {
    //         if(isDL()) dl.log("completeNewPasswordChallenge.onSuccess");
    //         callback.cognitoCallback(null, userAttributes);
    //       },
    //       onFailure: function (err) {
    //         if(isDL()) dl.log("completeNewPasswordChallenge.onFailure");
    //         callback.cognitoCallback(err, null);
    //       }
    //     });
    //   },
    //   onSuccess: function (result) {
    //     callback.cognitoCallback(null, result);
    //   },
    //   onFailure: function (err) {
    //     callback.cognitoCallback(err, null);
    //   }
    // });

    // cognitoUser.changePassword(newPasswordInfo.oldPassword, newPasswordInfo.newPassword, function (err, result) {
    //   if (err) {
    //     callback.cognitoCallback(err.message, null);
    //   } else {
    //     if(isDL()) dl.log('call result: ' + result);
    //     callback.cognitoCallback(null, result);
    //   }
    // });

    cognitoUser.authenticateUser(authenticationDetails, {
      onSuccess: function (result1) {
        if(isDL()) dl.log('authenticateUser() result: ' + result1);
        cognitoUser.changePassword(newPasswordInfo.oldPassword, newPasswordInfo.newPassword, function (err, result2) {
          if (err) {
            callback.cognitoCallback(err.message, null);
          } else {
            if(dl.isLoggable()) dl.log('changePassword() result: ' + result2);
            callback.cognitoCallback(null, result2);
          }
        });
      },
      onFailure: function (err) {
        callback.cognitoCallback(err, null);
      }
    });

  }


  logout() {
    if(isDL()) dl.log("UserLoginHelper: Logging out");
    this.ddbUtil.writeLogEntry("logout");
    this.cognitoUtil.getCurrentUser().signOut();

  }

}
