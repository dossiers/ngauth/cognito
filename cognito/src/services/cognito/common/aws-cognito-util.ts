export class AwsCognitoUtil {
  private constructor() {}

  public static getDefaultCognitoIdpEndpointUrl(region: string, userPoolId: string): string {
    let url = 'cognito-idp.' + region.toLowerCase() + '.amazonaws.com/' + userPoolId;
    return url;
  }

  public static getCognitoIdpEndpointUrl(idpEndpoint: string, userPoolId: string): string {
    let url = idpEndpoint + '/' + userPoolId;
    return url;
  }

}
