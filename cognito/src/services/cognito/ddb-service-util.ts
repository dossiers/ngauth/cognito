import { Injectable } from "@angular/core";

import * as AWS from "aws-sdk/global";
import * as DynamoDB from "aws-sdk/clients/dynamodb";

import { DevLogger as dl } from '@ngcore/core'; import isDL = dl.isLoggable;
import { LogItem } from '@ngauth/core';
import { CognitoCommonUtil } from './cognito-common-util';


@Injectable()
export class DdbServiceUtil {

  constructor(private cognitoUtil: CognitoCommonUtil) {
    // if(isDL()) dl.log("DynamoDBService: constructor");
  }

  private getAWS() {
    return AWS;
  }

  public getLogEntries(mapArray: Array<LogItem>) {
    if (isDL()) dl.log("DynamoDBService: reading from DDB with creds - " + AWS.config.credentials);
    var params = {
      TableName: this.cognitoUtil.authConfig.userIdTableName,
      KeyConditionExpression: "userId = :userId",
      ExpressionAttributeValues: {
        ":userId": this.cognitoUtil.getCognitoIdentity()
      }
    };

    var clientParams: any = {};
    if (this.cognitoUtil.authConfig.dynamoDbEndpoint) {
      clientParams.endpoint = this.cognitoUtil.authConfig.dynamoDbEndpoint;
    }
    var docClient = new DynamoDB.DocumentClient(clientParams);
    docClient.query(params, onQuery);

    function onQuery(err: AWS.AWSError, data: DynamoDB.DocumentClient.QueryOutput) {
      if (err) {
        console.error("DynamoDBService: Unable to query the table. Error JSON:", JSON.stringify(err, null, 2));
      } else {
        // print all the movies
        if (dl.isLoggable()) dl.log("DynamoDBService: Query succeeded.");
        data.Items.forEach(function (logitem) {
          mapArray.push({ type: logitem.type, date: logitem.activityDate });
        });
      }
    }
  }

  public writeLogEntry(type: string) {
    try {
      let date = new Date().toString();
      if (isDL()) dl.log("DynamoDBService: Writing log entry. Type:" + type + " ID: " + this.cognitoUtil.getCognitoIdentity() + " Date: " + date);
      this.write(this.cognitoUtil.getCognitoIdentity(), date, type);
    } catch (exc) {
      console.log("DynamoDBService: Couldn't write to DDB");
    }
  }

  private write(data: string, date: string, type: string): void {
    if (isDL()) dl.log("DynamoDBService: writing " + type + " entry");

    let clientParams: any = {
      params: { TableName: this.cognitoUtil.authConfig.userIdTableName }
    };
    if (this.cognitoUtil.authConfig.dynamoDbEndpoint) {
      clientParams.endpoint = this.cognitoUtil.authConfig.dynamoDbEndpoint;
    }
    var DDB = new DynamoDB(clientParams);

    // Write the item to the table
    var itemParams =
      {
        TableName: this.cognitoUtil.authConfig.userIdTableName,
        Item: {
          userId: { S: data },
          activityDate: { S: date },
          type: { S: type }
        }
      };
    DDB.putItem(itemParams, function (result) {
      if (dl.isLoggable()) dl.log("DynamoDBService: wrote entry: " + JSON.stringify(result));
    });
  }

}

