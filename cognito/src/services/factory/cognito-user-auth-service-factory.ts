import { Injectable } from "@angular/core";

import { IUserRegistrationService, IUserLoginService, IUserAttributesService } from '@ngauth/services';

import { UserAuthServiceFactory } from '@ngauth/services';

import { CognitoUserRegistrationService } from '../cognito/user-registration.service';
import { CognitoUserLoginService } from '../cognito/user-login.service';
import { CognitoUserAttributesService } from '../cognito/user-attributes.service';


@Injectable()
export class CognitoUserAuthServiceFactory extends UserAuthServiceFactory {

  constructor(
    private cognitoUserRegistrationService: CognitoUserRegistrationService,
    private cognitoUserLoginService: CognitoUserLoginService,
    private cognitoUserAttributesService: CognitoUserAttributesService
  ) {
    super();
  }

  getUserRegistrationService(): IUserRegistrationService {
    return this.cognitoUserRegistrationService;
  }

  getUserLoginService(): IUserLoginService {
    return this.cognitoUserLoginService;
  }
  getUserAttributesService(): IUserAttributesService {
    return this.cognitoUserAttributesService;
  }

}
